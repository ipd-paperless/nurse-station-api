import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import _ from 'lodash';
//import { Date Time } from 'luxon';
import { DoctorOrderModel } from '../../models/nurse/doctor_order';
import viewNurseNote from '../../schema/nurse/view_nurse_note';


export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db = fastify.db;
  const getDoctorOrder = new DoctorOrderModel();

  // get patient
  fastify.get('/:admit_id', {
    preHandler: [
      fastify.guard.role(['nurse', 'admin', 'doctor']),
      // fastify.guard.scope('nurse.create', 'admit.create', 'nurse.read', 'admit.read')
    ],
    schema: viewNurseNote
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const params: any = request.params;
      const { admit_id } = params;
      // let objdata: any = [];

      const data = await getDoctorOrder.getDoctorOrder(db, admit_id);

      return reply.status(StatusCodes.CREATED).send({ ok: true, data });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  // get doctor order group ปั้นก้อน data
  fastify.get('/order/:admit_id', {
    preHandler: [
      fastify.guard.role(['nurse', 'admin', 'doctor']),
      // fastify.guard.scope('nurse.create', 'admit.create', 'nurse.read', 'admit.read')
    ],
    schema: viewNurseNote
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const params: any = request.params;
      const { admit_id } = params;
  
      const getAdmitID: any[] = await getDoctorOrder.getDocterOrderadmitID(db, admit_id);

      for (let a of getAdmitID ) {
        
          let order = await getDoctorOrder.getOrderByID(db, a.id);
  
          if (order[0]) {
  
            a.order = order;
  
          } else {
            a.order = [];
  
          }
      }

      for (let a of getAdmitID ) {
        
        let progressNote = await getDoctorOrder.getProgressNoteByID(db, a.id);

        if (progressNote[0]) {

          a.progressNote = progressNote;

        } else {
          a.progressNote = [];

        }
    }

    for (let a of getAdmitID ) {
        
      let oneDay = await getDoctorOrder.getOnedayByID(db, a.id);

      if (oneDay[0]) {

        a.oneDay = oneDay;

      } else {
        a.oneDay = [];

      }
  }

  for (let a of getAdmitID ) {
        
    let continues = await getDoctorOrder.getContinueByID(db, a.id);

    if (continues[0]) {

      a.continues = continues;

    } else {
      a.continues = [];

    }
}









      
      return reply.status(StatusCodes.CREATED).send({ ok: true, getAdmitID });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  // post doctor_order
  fastify.post('/', {
    preHandler: [
      fastify.guard.role(['nurse', 'admin', 'doctor']),
      // fastify.guard.scope('nurse.create', 'admit.create', 'nurse.read', 'admit.read')
    ],
    schema: viewNurseNote
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const params: any = request.params;
      const { admit_id } = params;

      const data = await getDoctorOrder.getDoctorOrder(db, admit_id);

      return reply.status(StatusCodes.CREATED).send({ ok: true, data });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  done();

}

